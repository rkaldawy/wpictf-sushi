import random as rand

def buildBaseChar(ch):
    if ch is 'a':
        return 'cgiklmnoputy'
    elif ch is 'b':
        return 'abcdfjklmnptuvwx'
    elif ch is 'c':
        return 'abcdefkpuvwxy'
    elif ch is 'd':
        return 'abcdfjkoptuvwx'
    elif ch is 'e':
        return 'abcdefklmnopuvwxy'
    elif ch is 'f':
        return 'abcdefklmnpu'
    elif ch is 'g':
        return 'abcdefkmnoptuvwxy'
    elif ch is 'h':
        return 'aefjklmnoptuy'
    elif ch is 'i':
        return 'abcdehmruvwxy'
    elif ch is 'j':
        return 'abcdehmruvw'
    elif ch is 'k':
        return 'aefikmpsuy'
    elif ch is 'l':
        return 'afkpuvwxy'
    elif ch is 'm':
        return 'aefgijkmoptuy'
    elif ch is 'n':
        return 'aefgjkmopstuy'
    elif ch is 'o':
        return 'abcdefjkoptuvwxy'
    elif ch is 'p':
        return 'abcdefjklmnopu'
    elif ch is 'q':
        return 'abcdefjkmopstuvwxy'
    elif ch is 'r':
        return 'abcdefjklmnopsuy'
    elif ch is 's':
        return 'abcdefklmnotuvwxy'
    elif ch is 't':
        return 'abcdehmrw'
    elif ch is 'u':
        return 'aefjkoptuvwxy'
    elif ch is 'v':
        return 'aefjkoqsw'
    elif ch is 'w':
        return 'aefhjkmoprtvx'
    elif ch is 'x':
        return 'aegimqsuy'
    elif ch is 'y':
        return 'aegimrw'
    elif ch is 'z':
        return 'abcdeimquvwxy'
    elif ch is '_':
        return 'uvwxy'
    elif ch is '{':
        return 'bcdgkqvwx'
    elif ch is '}':
        return 'bcdiosvwx'
    else:
        raise Exception


def buildBaseString(str):
    return [buildBaseChar(ch) for ch in str]


def fillSegment(segment, minChars, maxChars):
    for i in range(97, 122):
        ch = chr(i)
        segment += "".join([ch for i in range(2*rand.randint(minChars, maxChars))])
    return segment

def fillSegments(segments, minChars, maxChars):
    return [fillSegment(seg, minChars, maxChars) for seg in segments]

def manipulateCaps(segs):
    for i in range(97, 122):
        ch = chr(i)
        opt = rand.randint(0, 5)
        if opt is 0: #half lower, half caps
            half = int(len(segs)/2)
            segs = [seg for seg in segs[0:half]] + [seg.replace(ch, ch.upper()) for seg in segs[half:]]
        if opt is 1: #half caps, half lower
            half = int(len(segs)/2) + 1
            segs = [seg.replace(ch, ch.upper()) for seg in segs[0:half]] + [seg for seg in segs[half:]]
        if opt is 2: # all caps
            segs = [seg.replace(ch, ch.upper()) for seg in segs]
        if opt is 3 or 4:
            segs = ["".join([ch.upper() if char is ch and rand.randint(0, 1) is 1 else char for char in seg]) for seg in segs]
        if opt is 5:
            pass
    return segs

def shuffle_word(word):
    word = list(word)
    rand.shuffle(word)
    return ''.join(word)

def scrambleSegs(segs):
    return [shuffle_word(word) for word in segs]

def buildFinalString(segs):
    return "".join([seg + 'z' for seg in segs])

def encodeString(input, minChars=10000, maxChars=100000):
    segs = buildBaseString(input)
    segs = fillSegments(segs, minChars, maxChars)
    segs = manipulateCaps(segs)
    segs = scrambleSegs(segs)
    out = buildFinalString(segs)
    return out

def encodeStringEasy(input):
    return encodeString(input.lower(), minChars=0, maxChars=2)

def writeToFile(string, name="encoded.txt"):
    f = open(name, "w")
    f.write(string)

#def main():
#    out = encodeString("wpictf{spicy_yellowtail_with_soy_wrap}")
#    writeToFile(out)

def main():
    while(True):
        text = input("Enter a string:")
        try:
            out = encodeStringEasy(text)
            print(out)
        except:
            print("You entered a string that can't be encoded!")



if __name__ == "__main__":
    main()

